package delfia.puja.day9

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DBMusik (context: Context): SQLiteOpenHelper(context,Db_name,null,Db_ver) {

    companion object{
        val Db_name = "musik"
        val Db_ver = 1
    }

    override fun onCreate(db: SQLiteDatabase?) {
        val tblmus = "create table musik(id_musik Int primary key , id_cov Int not null, judul text)"
        val inslist = "insert into musik(id_musik,id_cov,judul) values ('0x7f0p000','0x7f060039','Anne-Marie - 2002'),('0x7f0p001','0x7f060040','SHAUN Way Back Home'),('0x7f0p002','0x7f060041','Jay Park - All I Wanna Do')"

        db?.execSQL(tblmus)
        db?.execSQL(inslist)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}